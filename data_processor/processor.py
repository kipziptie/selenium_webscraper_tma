import numpy
from multiprocessing import Pool
from parsers.html_parser import Html_parser
from parsers.css_parser import Css_parser
from parsers.simple_parser import Simple_parser

"""
    The Processor is the class where all calculations regarding datasets are done.
"""

class Processor():
    """
            Input: Base dataset
    """
    def __init__(self, dataset):
        self.__ds = dataset
    
    """
        Compares between all given datasets.

            Input: List of Dataset objects

            Output: A dictionary of file names and a value between [0,1] 
                    representing the average similarity between all instances.
    """
    def compare_instances(self, datasets):
        files = set()
        for ds in datasets:
            files = files | set(ds.get_files())
        files = list(files)
        
        sim_dict = {}
        for file in files:
            if len(datasets) == 1:
                paths = ds.get_paths(file)
            else:
                paths = []
                for ds in datasets:
                    p = ds.get_paths(file)
                    if len(p) > 0:
                        paths.append(p[0])
            filetype = file.split('.')[-1]
            sim_dict[file] = self.compare_files(file, paths, filetype, datasets)
       
        return sim_dict

    """
        Compares between the given dataset and the base dataset.

            Input: Dataset to compare

            Output: A dictionary of file names and a value between [0,1] 
                    representing the average similarity between all instances.
    """
    def compare_dataset(self, ds):
        return self.compare_instances([self.__ds, ds])

    """
        Compares the different paths for a single file. If there is only one file,
        returns 0.

            Input:  name: name of the file
                    paths: List containing the paths (str) to each file to be computed
                    filetype: Type of file to be computed (HTML, CSS, other)
                    datasets: Datasets objects to compare

            Output: A value between [0,1] representing the average similarity between all instances.
    """
    def compare_files(self, name, paths, filetype, datasets):
                
        content = []
        try:
            for path in paths:
                for ds in datasets:
                    if path in ds.get_paths(name):
                        content.append(ds.load_file(name, path))
        except UnicodeDecodeError:
            print("Warning: could not load", name, '('+path+')')
            return 0
        
        values = []
        sim = 0
        for k, c1 in enumerate(content[:-1]):
            for c2 in content[k+1:]:

                # Filter 1: Element-wise comparison
                if c1 == c2:
                    sim = 1.0

                # Filter 2: Compare by filetype
                else:
                    if( filetype == "css" ):
                        # Parse CSS file, Extract all the rules and compare
                        css = Css_parser()
                        sim = css.compare(c1, c2)
                    
                    elif( filetype == "html" or c1[:15].upper() == "<!DOCTYPE HTML>"):
                        # Parse HTML file, Build DOM tree and compare nodes
                        html = Html_parser()
                        sim = html.compare(c1, c2)

                    else:
                        # Parse file, Tokenize items, and Compute similarity using FastDTW 
                        any_file = Simple_parser()
                        sim = any_file.compare(c1, c2)

                values.append(sim)
        
        if len(values) == 0:
            return 0
        return numpy.mean(values)

    """
        Compares the base dataset with itself.

            Output: A dictionary of file names and a value between [0,1] 
                    representing the average similarity between all instances.
    """
    def compare_self(self):
        return self.compare_instances([self.__ds])
    
    """
        Tries to find files with different names and equal content, for a fair comparison.

            Input: List of Dataset objects. Base dataset not included.

            Output: Tuple with:
                        List of files with equal names
                        List of files with same contents, but different names
                        List of files without a pair
                        List of files that could not be compared due to invalid characters
    """
    def file_difference(self, datasets):
        f_equal = []
        f_equal_notname = []
        f_different = []
        f_invalid = []
        if len(datasets) == 0:
            #Compare dataset with itself
            for file in self.__ds.get_files():
                paths = self.__ds.get_paths(file)
                if len(paths) > 1:
                    #File in more than one instance
                    f_equal.append(file)
                else:
                    path = paths[0]
                    try:
                        content = self.__ds.load_file(file, path)
                    except UnicodeDecodeError:
                        #Error
                        f_invalid.append(file)
                        continue
                    for file2 in self.__ds.get_files():
                        found = False
                        if file != file2 and not file2 in f_different and not file2 in f_invalid:
                            for path2 in self.__ds.get_paths(file2):
                                try:
                                    if content == self.__ds.load_file(file2, path2):
                                        #Coincidence found
                                        f_equal_notname.append((file, file2))
                                        found = True
                                        break
                                except UnicodeDecodeError:
                                    #Just skip it
                                    continue
                            if found:
                                break
                    else:
                        #None found equal
                        f_different.append(file)
                                
        else:
            datasets = list(datasets) + [self.__ds]
            for i,ds1 in enumerate(datasets):
                for ds2 in datasets:
                    if ds1 != ds2:
                        files1 = ds1.get_files()
                        files2 = ds2.get_files()
                        for file1 in files1:
                            if file1 in files2:
                                #File is in both datasets
                                f_equal.append(file1)
                            else:
                                #Check if any file in ds2 equals in content
                                    prev_content = ''
                                    for path1 in ds1.get_paths(file1):
                                        try:
                                            f1_content = ds1.load_file(file1, path1)
                                        except UnicodeDecodeError:
                                            f_invalid.append(file1)
                                            break
                                        if f1_content == prev_content:
                                            #Same contents as previous path, skip
                                            continue
                                        prev_content = f1_content
                                        
                                        found = False
                                        for file2 in files2:
                                            for path2 in ds2.get_paths(file2):
                                                try:
                                                    if f1_content == ds2.load_file(file2, path2):
                                                        f_equal_notname.append((file1, file2))
                                                        found = True
                                                        break
                                                except UnicodeDecodeError:
                                                    continue
                                            if found:
                                                break
                                        if found:
                                            break
                                    else:
                                        #No matches
                                        f_different.append(file1)
        return (f_equal, f_equal_notname, f_different, f_invalid)
