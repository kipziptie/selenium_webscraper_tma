import os

"""
    Dataset class organizes different instances with different files, granting
    access to their different paths and different content.
"""
class Dataset:
    """
            Input: Path of the dataset.
    """
    def __init__(self, path):
        #path -> dir
        #dir -> load list of files in each iteration
        self.__list = {}
        self.__n_of_instances = 0

        #self.__list {"index.html" : ["path1","path2", "path3"]
        for instance in os.listdir(path):
            self.__n_of_instances += 1
            for file in os.listdir(path+os.sep+instance):
                if not file in self.__list:
                    self.__list[file] = []
                self.__list[file].append(path+os.sep+instance+os.sep+file)
        
        self.__size = len(os.listdir(path))

    """
            Output: Number of instances.
    """
    def get_n_of_instances(self):
        return self.__n_of_instances

    """
            Output: Size of the dataset.
    """
    def get_size(self):
        return self.__size
    
    """
            Output: List of file names.
    """
    def get_files(self):
        return self.__list.keys()
    
    """
        Returns the paths of a given file.

            Input: Name of a file.

            Output: List of paths.
    """
    def get_paths(self, name):
        if not name in self.__list:
            return []
        return self.__list[name]
    
    """
        Reads a path of a file.
        
            Input:  name: Name of a file
                    path: Path of the file

            Output: String of the file.
    """
    def load_file(self, name, path):
        #load path file
        #return content
        if not name in self.__list or not path in self.__list[name]:
            return None
        with open(path, "r") as f:
            return f.read()
