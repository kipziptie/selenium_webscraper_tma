import os
import csv


file = open('../common/list_of_vpn.csv')
csvreader = csv.reader(file)
vpn_list = []
for row in csvreader:
    vpn_list.append(row[1])

DATA_DIR = "../data"
VPN_DIR = "VPN"

"""
    Pass from crawler structure to data_processor structure.
"""

if __name__ == '__main__':
    vpn_names = []
    
    for webpage in os.listdir(DATA_DIR):
        print(webpage)
        try:
            for vpn_instance in os.listdir(DATA_DIR+os.sep+webpage+os.sep+VPN_DIR):
                print(vpn_instance)
                i = int(vpn_instance)
                if i >= len(vpn_names):
                    for j in range(len(vpn_names),i+1):
                        vpn_names.append('')
                while vpn_names[i] == '' or vpn_names[i] in [x for j,x in enumerate(vpn_names) if j!=i]:#Not unique name
                    vpn_names[i] = vpn_list[i+1]#input("Insert name for vpn " + str(i) + ": ")
                try:
                    os.mkdir(DATA_DIR+os.sep+webpage+os.sep+vpn_names[i])
                    os.replace(DATA_DIR+os.sep+webpage+os.sep+VPN_DIR+os.sep+vpn_instance,\
                               DATA_DIR+os.sep+webpage+os.sep+vpn_names[i]+os.sep+'0')
                except FileExistsError:
                    print("Warning: Could not export vpn folder for", i,"("+vpn_names[i]+"): Folder already exists.")
            try:
                os.rmdir(DATA_DIR+os.sep+webpage+os.sep+VPN_DIR)
            except OSError:
                pass
        except FileNotFoundError:
            pass
            
