from dataset import Dataset
from processor import Processor
import os

import time

"""
    Main program for the processor. Run prev_step first!
"""

if __name__ == "__main__":
    for webpage in os.listdir(".."+os.sep+"data"):
        print("Checking web:", webpage)
        d = Dataset(".."+os.sep+"data"+os.sep+webpage+os.sep+"NO_VPN")
        p = Processor(d)
        
        print("\tComparing NOVPN with itself")
        
        time_start = time.time()
        sim_novpn_ds = p.compare_self()
        time_end = time.time()
        
        print("Time:", time_end-time_start)

        print("Difference in files")
        
        dif = p.file_difference(())
        
        print("Equal files:", len(dif[0]))
        #for f in dif[0]:
        #    print("\t",f)
        print("Equal files (different name):", len(dif[1]))
        for f in dif[1]:
            print(f[0], ", ", f[1])
        print("Different files:", len(dif[2]))
        for f in dif[2]:
            print(f)
        print("Invalid files:", len(dif[3]))
        for f in dif[3]:
            print(f)
        
        print()
        print("\tComparing NOVPN with VPN instances")
        
        for folder in os.listdir(".."+os.sep+"data"+os.sep+webpage):
            if folder != "NO_VPN":
                print("\t\tComparing with", folder)
                d2 = Dataset(".."+os.sep+"data"+os.sep+webpage+os.sep+folder)
                
                time_start = time.time()
                sim_vpn_ds = p.compare_dataset(d2)
                time_end = time.time()
                print("Time:", time_end-time_start)
                
                files_novpn_ds = d.get_files()
                files_vpn_ds = d2.get_files()
                for file in files_novpn_ds:
                    if sim_novpn_ds[file] != 0 and file in files_vpn_ds:
                        sim_file = (sim_novpn_ds[file], sim_vpn_ds[file])
                        if sim_file[0] != sim_file[1]:
                            print(file, sim_file)
                print()
                print("Difference in files")
                dif = p.file_difference((d2,))
                print("Equal files:", len(dif[0]))
                #for f in dif[0]:
                #    print("\t",f)
                print("Equal files (different name):", len(dif[1]))
                for f in dif[1]:
                    print("\t",f[0], ", ", f[1])
                print("Different files:", len(dif[2]))
                for f in dif[2]:
                    print("\t",f)
                print("Invalid files:", len(dif[3]))
                for f in dif[3]:
                    print("\t",f)
                print()
        print()
        print()
