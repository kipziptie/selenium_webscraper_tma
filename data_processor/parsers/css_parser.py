import tinycss

class Css_parser():
    def __init__(self):
        self.__parser = tinycss.make_parser("page3")

    def __jaccard_index(self, s1, s2):
        return( len(s1 & s2) / len(s1 | s2) )

    def compare(self, css1, css2):
        # Parse css files
        stylesheet = parser.parse_stylesheet(css1)
        stylesheet2 = parser.parse_stylesheet(css2)

        # Build set of rules
        r1 = {rule.selector.as_css() for rule in stylesheet.rules}
        r2 = {rule.selector.as_css() for rule in stylesheet2.rules}

        # Compute similarity
        return self.__jaccard_index(r1, r2)