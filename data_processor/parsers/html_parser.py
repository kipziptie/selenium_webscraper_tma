import subprocess
import sys
from lxml import html
from zss import simple_distance, Node
import networkx as nx

class Html_parser():

    def __make_html_zssgraph(self, parent, graph=None, ignore_comments=True):
        ''' Given a string containing HTML, return a zss style tree of the DOM '''
        if not graph:
          graph = Node(parent.tag)
        for node in parent.getchildren():
          # if the element is a comment, ignore it
          if ignore_comments and not isinstance(node.tag, str):
            continue
          graph.addkid(Node(node.tag))
          self.__make_html_zssgraph(node, graph)
        return graph

    def __domain_to_graph(self, content):
        ''' a wrapper function that turns an html file to a dom graph '''
        html_tag = html.document_fromstring(content)
        return self.__make_html_zssgraph(html_tag)

    def __compare_graphs(self, g1, g2):
        ''' Given two graphs (zss trees) return a normalized distance between them '''
        g1_node_count = float( len(g1.get_children(g1)) ) # get the length of the tree starting at the root
        g2_node_count = float( len(g2.get_children(g2)) ) # ''
        
        dist = float(simple_distance(g1, g2))
        return 1 - (dist / (g1_node_count + g2_node_count))

    def compare(self, html1, html2):
        g1 = self.__domain_to_graph(html1)
        g2 = self.__domain_to_graph(html2)
        return(self.__compare_graphs(g1, g2))