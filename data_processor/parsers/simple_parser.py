import re
from fastdtw import fastdtw

class Simple_parser():

    def __tokenize(self, str_):
        tokens = re.split('\W+', str_)
        int_tokens =[hash(item) for item in tokens]
        return int_tokens

    def __bin_dist(self, x, y):
        return 0 if x==y else 1

    def __similarity(self, dist, arr1, arr2):
        return 1 - dist/max(len(arr1),len(arr2))

    def compare(self, f1, f2):
        # Split the input into hashed integer tokens
        tokens1 = self.__tokenize(f1)
        tokens2 = self.__tokenize(f2)

        # Compute Dynamic Time Warping Distance
        dist, _ = fastdtw(tokens1, tokens2, dist=self.__bin_dist)

        # Return Similarity
        return self.__similarity(dist, tokens1, tokens2)