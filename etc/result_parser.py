import re

WEB_PREFIX = 'Checking web: '
VPN_PREFIX = '\t\tComparing with '

TIME_PREFIX = 'Time: '
EQUAL_PREFIX = 'Equal files: '
NAME_PREFIX = 'Equal files (different name): '
DIFFERENT_PREFIX = 'Different files: '
INVALID_PREFIX =  'Invalid files: '


if __name__ == '__main__':

    state = 0

    websites = {}
    vpns = ['none']

    webdiff = {}
    
    w = ''
    v = ''
    vpos = 0
    
    eq = 0
    enq = 0
    df = 0
    ef = 0
    
    with open('result_all.txt', 'r') as f:
        for line in f:
            if state == 0:
                #Read webite
                if line.startswith(WEB_PREFIX):
                    w = line[len(WEB_PREFIX):-1]
                    #if not w in websites:
                    websites[w] = []
                    state += 1
            elif state == 1:
                #Equal files
                if line.startswith(EQUAL_PREFIX):
                    eq = int(line[len(EQUAL_PREFIX):-1])
                    state += 1
            elif state == 2:
                #Equal files diff name
                if line.startswith(NAME_PREFIX):
                    enq = int(line[len(NAME_PREFIX):-1])
                    state += 1
            elif state == 3:
                #Diff files
                if line.startswith(DIFFERENT_PREFIX):
                    df = int(line[len(DIFFERENT_PREFIX):-1])
                    state += 1
            elif state == 4:
                #Invalid files
                if line.startswith(INVALID_PREFIX):
                    ef = int(line[len(INVALID_PREFIX):-1])
                    websites[w].append((0, eq, enq, df, ef))
                    state += 1
            elif state == 5:
                #VPN name
                if line.startswith(VPN_PREFIX):
                    v = line[len(VPN_PREFIX):-1]
                    try:
                        vpos = vpns.index(v)
                    except ValueError:
                        vpos = len(vpns)
                        vpns.append(v)
                    
                    state += 1
            elif state == 6:
                #File differences start after time
                if line.startswith(TIME_PREFIX):
                    state += 1
            elif state == 7:
                #File differences
                if line == '\n':
                    state += 1
                else:
                    if not w in webdiff:
                        webdiff[w] = []
                    m = re.match(r"(\S+) \((\d+\.\d+), (\d+\.\d+)\)", line)
                    webdiff[w].append((vpos, m.group(1), float(m.group(2)), float(m.group(3))))
            elif state == 8:
                #Equal files
                if line.startswith(EQUAL_PREFIX):
                    eq = int(line[len(EQUAL_PREFIX):-1])
                    state += 1
            elif state == 9:
                #Equal files diff name
                if line.startswith(NAME_PREFIX):
                    enq = int(line[len(NAME_PREFIX):-1])
                    state += 1
            elif state == 10:
                #Diff files
                if line.startswith(DIFFERENT_PREFIX):
                    df = int(line[len(DIFFERENT_PREFIX):-1])
                    state += 1
            elif state == 11:
                #Invalid files
                if line.startswith(INVALID_PREFIX):
                    ef = int(line[len(INVALID_PREFIX):-1])
                    websites[w].append((vpos, eq, enq, df, ef))
                    state += 1
            elif state >= 12 and state <= 14:
                #Three spaces between webs
                #If not, is another vpn
                if line == '\n':
                    state += 1
                    if state == 15:
                        state = 0
                elif line.startswith(VPN_PREFIX):
                    v = line[len(VPN_PREFIX):-1]
                    try:
                        vpos = vpns.index(v)
                    except ValueError:
                        vpos = len(vpns)
                        vpns.append(v)
                    state = 6
    
    with open('parsed.csv', 'w') as f:
        f.write('web,vpn,equal files,diff name,diff file,invalid file\n')
        for web in websites:
            for t in websites[web]:
                f.write(web+','+vpns[t[0]]+','+str(t[1])+','+str(t[2])+','+str(t[3])+','+str(t[4])+'\n')

    with open('differences.csv', 'w') as f:
        f.write('web,vpn,file name,original comparison,vpn comparison\n')
        for web in webdiff:
            for t in webdiff[web]:
                f.write(web+','+vpns[t[0]]+','+t[1]+','+str(t[2])+','+str(t[3])+'\n')
