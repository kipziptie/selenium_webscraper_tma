# The purpose of this script is to choose all Chrome Extensions which are likely to be VPN apps.
# The selection criteria is if the chrome extension has the string "VPN" in the name

import json
FILENAME="../VPN_LIST/extensions-2021.json" # https://github.com/DebugBear/chrome-extension-list/blob/master/extensions-2021.json
                                         # https://www.debugbear.com/blog/counting-chrome-extensions

with open(FILENAME, "r") as data:
    extensions = json.load(data)

vpn_count = 0
extension_count = 0

for extension in extensions:
    if "vpn" in extension["name"].lower():
        vpn_count += 1
    extension_count += 1

print("Total Extensions:", extension_count,"\nTotal VPN:", vpn_count)

# Define a function to filter out completed TODOs
# of users with max completed TODOS.
def is_vpn(extension):
    vpn_in_title = "VPN" in extension["name"]
    return vpn_in_title

# Write filtered TODOs to file.
with open("vpn_list.json", "w") as data_file:
    filtered_extensions = list(filter(is_vpn, extensions))
    json.dump(filtered_extensions, data_file, indent=2)