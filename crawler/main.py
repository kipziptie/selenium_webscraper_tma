from csv import reader
from selenium.webdriver import Chrome
from pathlib import Path
import os
import requests
from json import loads as jloads
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from time import sleep, time
import re
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.action_chains import ActionChains

# we need pyautogui to interact with chrome outside of the HTML render window.
# Selenium is not capable of interacting with the native GUI, apparently by design..
# in order to use the image search functionality, you must install scrot. sudo apt-get install scrot
import pyautogui

#from common.common import get_website_list

import sys  
sys.path.insert(0, '../common/')
from common import get_website_list
#sitelist = Path("list_of_websites.csv")

def get_chrome_extensions():
    return os.listdir(EXTENSION_DIR)

###########################################################
# GLOBALS
sitelist = Path("../common/list_of_websites.csv")
N_DOMAINS = 0
DOWNLOAD_DIR = "../data"
VPN_PROMPT =False
EXTENSION_DIR = "./crx"
EXTENSION_LIST = get_chrome_extensions()
###########################################################

def start_chrome(extension = ""):

    d = DesiredCapabilities.CHROME
    d['loggingPrefs'] = { 'performance':'ALL' }
    chrome_options = Options()
    chrome_options.add_experimental_option('w3c', False)
    prefs = {"profile.default_content_setting_values.notifications" : 2}
    chrome_options.add_experimental_option("prefs", prefs)
    chrome_options.add_argument("start-maximized")
    # Add a ChromeDriver-specific capability.

    if (VPN_PROMPT == True):
        # Start the chromedriver with the VPN extension
        # VPN extension is popped from the list so it is not used again
        chrome_options.add_extension(Path(EXTENSION_DIR,EXTENSION_LIST.pop()));
    driver = webdriver.Chrome(desired_capabilities=d, options=chrome_options, executable_path='/usr/bin/chromedriver')

    if (VPN_PROMPT == True):
        print("\n##########################################################################")
        print("\nWould you like to enable a VPN during this run? if so - turn it on now\n")
        print("Press RETURN to continue...\n")
        print("##########################################################################\n")
        getUserInput = input()
    return driver

def startup_check():
    if not os.path.exists("../data/"):
        os.mkdir("../data/")
    if not os.path.exists(str(DOWNLOAD_DIR)):
        os.mkdir(DOWNLOAD_DIR)

def download_file(url, destination, headers=None, verify=True):
    h = {
        "User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:45.0) Gecko/20100101 Firefox/45.0"
    }
    if headers is not None:
        h.update(headers)

    resp = requests.get(url, stream=True, headers=h, timeout=(6, 27), verify=verify)
    for chunk in resp.iter_content(chunk_size=4096):
        if chunk:
            destination.write(chunk)

    destination.seek(0)
    return destination, resp.headers

def get_filetype(filename):
    tokens = re.split('\W+', filename)
    if "js" in tokens:
        return ".js"
    elif "html" in tokens:
        return  ".html"
    elif "css" in tokens:
        return ".css"
           
    return ""

def download_resources(index, request, saveFolder, fileName):
    # BREAK-FIX
    # If the requested resource is missing the URL paramenter - Skip It. 
    if not "url" in request:
        return

    # Parsing the filename out of the request path    
    filename = request["url"].split('/')[-1]
    if filename == '':
        filename = "index.html"
        
    if len(filename) > 124:
        filename = str(hash(filename)) + get_filetype(filename)

    elif filename.split('.')[-1] not in ["html", "js", "css"]:
        filename = filename + get_filetype(filename)

    print("###\n"+request["url"]+"###\n")
    # If the filename is blank, that means we are attempting to download the home page
    #print(request["url"].split('/'))

    print(request["url"])
    if "mime_type" in request and any(
            case in request["mime_type"]
            for case in ("text/", "application/x-javascript", "application/javascript")
    ):
        filename = os.path.join(
            DOWNLOAD_DIR, saveFolder, str(filename)
        )

        with open(filename, "wb") as f:
            try:
                f, headers = download_file(url=request["url"], destination=f)
            except requests.exceptions.SSLError:
                try:
                    requests.packages.urllib3.disable_warnings()
                    f, headers = download_file(
                        url=request["url"], destination=f, verify=False
                    )
                except Exception as e:
                    print("Error #1: %s" % (str(e)))
            except UnicodeError as e:
                print(
                    "Error #2: Couldn't download url %s with error %s"
                    % (request["url"], str(e))
                )
            except Exception as e:
                print("Error #3: %s" % (str(e)))
            print(
                "Found external resource %s. Saving file %s"
                % (request["url"], filename)
            )
        if os.path.getsize(filename) == 0:
            os.remove(filename)
            print("Deleted file %s for it was empty!" % filename)

def get_network(log_entries):
    network_traffic = {}
    for log_entry in log_entries:
        message = jloads(log_entry["message"])
        method = message["message"]["method"]
        params = message["message"]["params"]
        if method not in [
            "Network.requestWillBeSent",
            "Network.responseReceived",
            "Network.loadingFinished",
        ]:
            continue
        if method != "Network.loadingFinished":
            request_id = params["requestId"]
            loader_id = params["loaderId"]
            if loader_id not in network_traffic:
                network_traffic[loader_id] = {
                    "requests": {},
                    "encoded_data_length": 0,
                }
            if request_id == loader_id:
                if "redirectResponse" in params:
                    network_traffic[loader_id]["encoded_data_length"] += params[
                        "redirectResponse"
                    ]["encodedDataLength"]
                if method == "Network.responseReceived":
                    network_traffic[loader_id]["type"] = params["type"]
                    network_traffic[loader_id]["url"] = params["response"]["url"]
                    network_traffic[loader_id]["remote_IP_address"] = None
                    if "remoteIPAddress" in params["response"].keys():
                        network_traffic[loader_id]["remote_IP_address"] = params[
                            "response"
                        ]["remoteIPAddress"]
                    network_traffic[loader_id]["encoded_data_length"] += params[
                        "response"
                    ]["encodedDataLength"]
                    network_traffic[loader_id]["headers"] = params["response"][
                        "headers"
                    ]
                    network_traffic[loader_id]["status"] = params["response"][
                        "status"
                    ]
                    network_traffic[loader_id]["security_state"] = params[
                        "response"
                    ]["securityState"]
                    network_traffic[loader_id]["mime_type"] = params["response"][
                        "mimeType"
                    ]
                    if "via" in params["response"]["headers"]:
                        network_traffic[loader_id]["cached"] = True
            else:
                if request_id not in network_traffic[loader_id]["requests"]:
                    network_traffic[loader_id]["requests"][request_id] = {
                        "encoded_data_length": 0
                    }
                if "redirectResponse" in params:
                    network_traffic[loader_id]["requests"][request_id][
                        "encoded_data_length"
                    ] += params["redirectResponse"]["encodedDataLength"]
                if method == "Network.responseReceived":
                    network_traffic[loader_id]["requests"][request_id][
                        "type"
                    ] = params["type"]
                    network_traffic[loader_id]["requests"][request_id][
                        "url"
                    ] = params["response"]["url"]
                    network_traffic[loader_id]["requests"][request_id][
                        "remote_IP_address"
                    ] = None
                    if "remoteIPAddress" in params["response"].keys():
                        network_traffic[loader_id]["requests"][request_id][
                            "remote_IP_address"
                        ] = params["response"]["remoteIPAddress"]
                    network_traffic[loader_id]["requests"][request_id][
                        "encoded_data_length"
                    ] += params["response"]["encodedDataLength"]
                    network_traffic[loader_id]["requests"][request_id][
                        "headers"
                    ] = params["response"]["headers"]
                    network_traffic[loader_id]["requests"][request_id][
                        "status"
                    ] = params["response"]["status"]
                    network_traffic[loader_id]["requests"][request_id][
                        "security_state"
                    ] = params["response"]["securityState"]
                    network_traffic[loader_id]["requests"][request_id][
                        "mime_type"
                    ] = params["response"]["mimeType"]
                    if "via" in params["response"]["headers"]:
                        network_traffic[loader_id]["requests"][request_id][
                            "cached"
                        ] = 1
        else:
            request_id = params["requestId"]
            encoded_data_length = params["encodedDataLength"]
            for loader_id in network_traffic:
                if request_id == loader_id:
                    network_traffic[loader_id][
                        "encoded_data_length"
                    ] += encoded_data_length
                elif request_id in network_traffic[loader_id]["requests"]:
                    network_traffic[loader_id]["requests"][request_id][
                        "encoded_data_length"
                    ] += encoded_data_length
    return network_traffic

def clear_data(driver):
    driver.delete_all_cookies()
    # Obtained from
    # https://stackoverflow.com/questions/50456783/python-selenium-clear-the-cache-and-cookies-in-my-chrome-webdriver
    #driver.execute_script("window.open('');")
    #sleep(0.25)
    #driver.switch_to.window(driver.window_handles[-1])
    #sleep(0.25)
    driver.get(
        "chrome://settings/clearBrowserData"
    )  # for old chromedriver versions use cleardriverData
    #sleep(3)
    actions = ActionChains(driver)
    actions.send_keys(Keys.TAB * 2 + Keys.DOWN * 4)  # send right combination
    actions.perform()
    #sleep(0.5)
    actions = ActionChains(driver)
    actions.send_keys(Keys.TAB * 5 + Keys.ENTER)  # confirm
    actions.perform()
    #sleep(1)  # wait some time to finish. MIGHT BE USELESS
    #driver.close()  # close this tab
    #driver.switch_to.window(driver.window_handles[0])  # switch back

if __name__ == '__main__':
    startup_check()

    while EXTENSION_LIST:
        driver=start_chrome()
        myList = get_website_list(sitelist)

        for URL in myList:
            clear_data(driver)
            print(URL)
            driver.get("https://"+URL)
            log_entries = driver.get_log("performance")
            network_traffic = get_network(log_entries)

            # Create download folder
            download_dir = (
                    DOWNLOAD_DIR +"/"+ URL + "/"
            )
            if(VPN_PROMPT):
                    download_dir += "VPN"
            else:
                    download_dir += "NO_VPN"

            os.makedirs(download_dir, exist_ok=True)

            count = 0
            while os.path.exists(str(download_dir+"/"+str(count)+"/")):
                count += 1
            download_dir += "/"+str(count)+"/"
            os.mkdir(download_dir)

            fileName = 1
            for key in network_traffic.keys():
                download_resources(myList.index(URL), network_traffic[key],download_dir,fileName)
                fileName += 1
                for sub_key in network_traffic[key]["requests"].keys():

                    download_resources(
                        myList.index(URL), network_traffic[key]["requests"][sub_key],download_dir, fileName
                    )
                    fileName += 1


        driver.quit()
    print("\nSUCCESSFUL RUN! yay.")
    print("TODO - Doublecheck that cookies and history are cleared after sleep removal")
