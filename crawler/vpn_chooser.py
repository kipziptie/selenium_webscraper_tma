# The purpose of this file is to choose the top N VPN apps from a JSON list of VPN apps.
# The list is assumed to be sorted by "Most Downloaded"
import json
FILENAME="vpn_list.json" # https://github.com/DebugBear/chrome-extension-list/blob/master/extensions-2021.json
                                         # https://www.debugbear.com/blog/counting-chrome-extensions
TOP_N_VPNS=25

with open(FILENAME, "r") as data:
    vpns = json.load(data)

count=0
for vpn in vpns:
    if count >= TOP_N_VPNS:
        break

    print(vpn["name"], "\n", vpn["url"])
    count += 1