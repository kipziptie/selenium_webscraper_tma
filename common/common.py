from pathlib import Path

def get_website_list(filepath: str) -> list:
    """This method parses a document into a list object that contains the urls.

    The file must use the ',' character as a separator, if values describing that URL are to be passed in later

    :param filepath: Path to the .csv file that contains the list.

    :return: A list of URLs, as strings.

    :note: This feature is not currently parsing the file as a CSV. Just a newline separated list of URLs
    """
    clean_list = []

    if Path(filepath).exists():
        with open(filepath, "r") as f:
            raw_list = f.readlines()

        #raw_list.pop(0)
        N_DOMAINS = len(raw_list)
        for row in raw_list:
            field = row.strip()
            clean_list.append(field)
    return clean_list


